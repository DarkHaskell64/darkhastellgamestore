
package com.DarkHaskell.GamesDHaskell.Controller;

import com.DarkHaskell.GamesDHaskell.Domain.Videojuego;
import com.DarkHaskell.GamesDHaskell.Service.VideojuegoService;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ListadoContorller {
    private final VideojuegoService videoJuegoservice;  

    public ListadoContorller(VideojuegoService videoJuegoservice) {
        this.videoJuegoservice = videoJuegoservice;
    }
    
    
    @RequestMapping("/")
    public String listarObjetos(Model model ){
        List<Videojuego> destacados = videoJuegoservice.buscarDestacados();
        model.addAttribute("videojuegos", destacados);
        
        for(Videojuego vj: destacados){
            System.out.println("Nombre del Juego: " + vj.getNombre());
        }
        return "listado";
    }
 
    
    @RequestMapping("/videojuegosPorDistribuidor")
    public String listarVideojuegosPorDistribuidor(int distribuidorId,Model model){
        List<Videojuego> destacadosPorDistri = videoJuegoservice.buscarPorDistribuidor(distribuidorId);
        model.addAttribute("videojuegos", destacadosPorDistri);
        
        for(Videojuego vj: destacadosPorDistri){
            System.out.println("Nombre del Juego: " + vj.getNombre());
        }
        return "listado"; // retorno listado, que es el nombre del html.
    }
    
    @RequestMapping("/buscar")
    public String listarVideojuegosPorBusqueda(@RequestParam("q") String consulta,Model model){
        List<Videojuego> videjuegosfiltrados = videoJuegoservice.buscar(consulta);
        model.addAttribute("videojuegos", videjuegosfiltrados);
        
        for(Videojuego vj: videjuegosfiltrados){
            System.out.println("Nombre del Juego: " + vj.getNombre());
        }
        return "listado"; // retorno listado, que es el nombre del html.
    }
}
