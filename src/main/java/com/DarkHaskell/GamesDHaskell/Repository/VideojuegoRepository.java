package com.DarkHaskell.GamesDHaskell.Repository;

import com.DarkHaskell.GamesDHaskell.Domain.Distribuidor;
import com.DarkHaskell.GamesDHaskell.Domain.Videojuego;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface VideojuegoRepository extends JpaRepository<Videojuego, Integer>{
    
    //@Query(value = "select * from videojuego  order by nombre", nativeQuery = true) SQL NATIVE
    @Query(" from Videojuego v order by v.nombre") // Setencia en JPQL, leguanje propio de JPA. Se asocia mas a la clase y sus atributos.
    List<Videojuego> buscarTodos();   
    
    @Query("from Videojuego v where v.distribuidor.id = ?1 order by v.nombre") /**?1 hace referencia al primer parametro del metodo**/
    List<Videojuego> buscarPorDistribuidor(int distribuidorID);
    
    //@Query("from Videojuego v where v.nombre like %?1%")
    List<Videojuego> findByNombreContaining(String consulta);
}
