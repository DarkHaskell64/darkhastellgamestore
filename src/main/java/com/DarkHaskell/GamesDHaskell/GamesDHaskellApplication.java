package com.DarkHaskell.GamesDHaskell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamesDHaskellApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamesDHaskellApplication.class, args);
	}

}
