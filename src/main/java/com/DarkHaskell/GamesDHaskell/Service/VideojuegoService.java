package com.DarkHaskell.GamesDHaskell.Service;

import com.DarkHaskell.GamesDHaskell.Domain.Videojuego;
import com.DarkHaskell.GamesDHaskell.Repository.VideojuegoRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class VideojuegoService {
    private final VideojuegoRepository repository;

    public VideojuegoService(VideojuegoRepository repository) {
        this.repository = repository;
    }
    
    
    
    public List<Videojuego> buscarDestacados(){
        return repository.buscarTodos();
    }
    
    public List<Videojuego> buscarPorDistribuidor(int distribuidorID){
        return repository.buscarPorDistribuidor(distribuidorID);
    }
    
      public List<Videojuego> buscar(String consulta){
        return repository.findByNombreContaining(consulta);
    }
}
