
DROP TABLE IF EXISTS videojuego,distribuidor,desarrollador;

CREATE TABLE videojuego(
    id int primary key auto_increment,
    nombre Varchar(255) not null,
    descripcion Varchar(3000),
    imagenUrl Varchar(1000) not null,
    categoria Varchar(400) not null
);

INSERT INTO videojuego(id,nombre,descripcion,imagenUrl,categoria)
VALUES (1,'Dead Space', 'Uno de los juegos de terror más horripilante del mercado. Estás solo en una nave espacial con miles de criaturas que querran comerte!', 'https://i2.wp.com/culturageek.com.ar/wp-content/uploads/2019/03/WhaOiMH.jpg?resize=1000%2C625', 'Terror'),
 (2,'OutLast', 'Juego de terror. Encarnaras a un pobre  desgraciado que tendra que escapar de un manicomio con su camara como unica compañera. ¿Podrás Escapar?', 'https://cdn02.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_download_software_1/H2x1_NSwitchDS_OutlastBundleOfTerror_image1600w.jpg', 'Terror'),
 (3,'GTA 5','Simulador de la vida real, bueno si eres un psicopata, asesino, violento y pervertido','https://d2skuhm0vrry40.cloudfront.net/2013/articles//a/1/6/1/2/8/6/2/eurogamer-w8khik.jpg/EG11/thumbnail/750x422/format/jpg/quality/60','Mundo Abierto'),
 (4,'Miscreated','Juegazo de supervivencia. Nadie es tu amigo en este mundo postapocalíptico, ni los pollitos. Procura sobrevivir!','https://http2.mlstatic.com/miscreated-pc-steam-original-entrega-inmediata-D_NQ_NP_443215-MLA25142205695_112016-F.webp','Mundo Abierto, Supervivencia'),
 (5,'FIFA 20','Uno de lo juegos de deportes mas jugados de la década. Convertite en el Leo Messi de los deportes electronicos.','https://static3.a24.com/images/2019/10/28/r1kLKq49S-768x000.jpeg','Deportes'),
 (6,'Cities Skylines','Crea tu ciudad, con tus reglas, leyes y sobre todo tus negocios no tan legales.','https://http2.mlstatic.com/cities-skylines-v112-todos-sus-dlcs-pc-digital-D_NQ_NP_943960-MLA32832329554_112019-F.jpg','Simulacion'),
 (7,'The Witcher 3 ','El juego del año. Encarnarás a un witcher de pura sepa. Magia, mujeres y moustros, este juego lo tiene todo.','https://cdn02.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_4/H2x1_NSwitch_TheWitcher3WildHuntCompleteEdition_enGB_image1600w.jpg','Mundo Abierto fantástico'),
 (8,'Monster Hunter World','El juego de cazar moustros mas conocido y jugado del mundo. ¿Serás capaz de cazar a estas betias?...','https://steamcdn-a.akamaihd.net/steam/apps/582010/header.jpg?t=1580432965','Mundo Abierto, Fantasia');

CREATE TABLE distribuidor(
 id int primary key auto_increment,
 nombre Varchar(300) not null,
 sitio_web Varchar(600)
);

INSERT INTO distribuidor(id,nombre,sitio_web)
VALUES(1,'EA','https://www.ea.com/'),
(2,'Red Barrels Game','http://redbarrelsgames.com/'),
(3,'RockStar Games','https://www.rockstargames.com/'),
(4,'Entrada Interactive','https://entradainteractive.com/'),
(5,'Paradox Interactive','https://www.paradoxinteractive.com/en/'),
(6,'CDProject Red','https://en.cdprojektred.com/'),
(7,'CAPCOM','http://www.capcom.com/');

ALTER TABLE videojuego
ADD COLUMN distribuidor_id INT,
ADD FOREIGN KEY (distribuidor_id) REFERENCES distribuidor(id);

UPDATE videojuego Set distribuidor_id = 1 where id IN(1,5);
UPDATE videojuego Set distribuidor_id = 2 where id = 2;
UPDATE videojuego Set distribuidor_id = 3 where id = 3;
UPDATE videojuego Set distribuidor_id = 4 where id = 4;
UPDATE videojuego Set distribuidor_id = 5 where id = 6;
UPDATE videojuego Set distribuidor_id = 6 where id = 7;
UPDATE videojuego Set distribuidor_id = 7 where id = 8;



ALTER TABLE videojuego
modify distribuidor_id int not null;

CREATE TABLE desarrollador(
 id int primary key auto_increment,
 nombre Varchar(300) not null,
 imagen_url Varchar(600)
);


INSERT INTO desarrollador(id,nombre,imagen_url)
VALUES(1,'Visceral Games ','https://hardwaresfera.com/wp-content/uploads/2017/11/visceral-games-dead-space.jpg'),
(2,'Red Barrels Game','https://www.redbarrelsgames.com/img/RedBarrels-logo.jpg'),
(3,'RockStar North','https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Rockstar_North_Logo.svg/1200px-Rockstar_North_Logo.svg.png'),
(4,'Entrada Interactive','https://pbs.twimg.com/media/CrCsq2CUMAAdLrc.jpg'),
(5,'EA Vancouver','https://thumbnails.pcgamingwiki.com/c/c8/EA_Vancouver_logo.png/300px-EA_Vancouver_logo.png'),
(6,'Paradox Interactive','https://www.paradoxplaza.com/on/demandware.static/Sites-Paradox_US-Site/-/default/dwd9bf9c44/android-chrome-512x512.png'),
(7,'CDProject RED','https://i.blogs.es/6a3e76/the-witcher-3-wild-hunt-pc-playstation-4-xbox-one_228912/840_560.jpg'),
(8,'CAPCOM','https://gamedustria.com/wp-content/uploads/2019/08/F431307C-DCFD-4906-BCEA-A036EA17FA23-1080x675.jpeg');

ALTER TABLE videojuego
ADD COLUMN desarrollador_id INT,
ADD FOREIGN KEY (desarrollador_id) REFERENCES desarrollador(id);

UPDATE videojuego Set desarrollador_id = 1 where id = 1;
UPDATE videojuego Set desarrollador_id = 2 where id = 2;
UPDATE videojuego Set desarrollador_id = 3 where id = 3;
UPDATE videojuego Set desarrollador_id = 4 where id = 4;
UPDATE videojuego Set desarrollador_id = 5 where id = 5;
UPDATE videojuego Set desarrollador_id = 6 where id = 6;
UPDATE videojuego Set desarrollador_id = 7 where id = 7;
UPDATE videojuego Set desarrollador_id = 8 where id = 8;



ALTER TABLE videojuego
modify desarrollador_id int not null;
